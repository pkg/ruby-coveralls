Source: ruby-coveralls
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <nomadium@debian.org>,
           Lucas Kanashiro <kanashiro@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               ronn,
               ruby-multi-json,
               ruby-rest-client,
               ruby-ronn,
               ruby-rspec,
               ruby-simplecov,
               ruby-simplecov-html,
               ruby-term-ansicolor,
               ruby-thor,
               ruby-webmock,
               ruby-vcr
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-coveralls.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-coveralls
Homepage: https://coveralls.io
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-coveralls
Architecture: all
Depends: ruby | ruby-interpreter,
         ruby-multi-json,
         ruby-rest-client,
         ruby-simplecov,
         ruby-simplecov-html,
         ruby-term-ansicolor,
         ruby-thor,
         ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: Ruby implementation of the Coveralls API
 Coveralls is a web service to help you track your code coverage over
 time, and ensure that all your new code is fully covered.
 .
 Coveralls automatically collects your code coverage data, uploads it
 to their servers and gives you a nice interface to dig into it.
 .
 Any type of Ruby project or test framework supported by SimpleCov is
 supported by the Coveralls gem. This includes all your favorites, like
 RSpec, Cucumber, and Test::Unit.
 .
 This package provides a Ruby gem to interact with Coveralls API.
